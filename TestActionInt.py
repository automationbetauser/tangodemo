def handler(context, inputs):
    greeting = "Hello new version, {0}!".format(inputs["target"])
    print(greeting)

    outputs = {
      "greeting": greeting
    }

    return outputs
