# security groups
resource "aws_security_group" "cluster" {
  name        = "${var.clusterName}-temp-xyz-cluster"
  description = "Cluster communication with worker nodes"
  vpc_id      = var.create_vpc ? aws_vpc.cluster[0].id : data.aws_vpc.vpc[0].id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = merge(
    local.tags,
    {
      "Name" = "${var.clusterName}-temp-xyz"
    },
  )
}

resource "aws_security_group_rule" "cluster-ingress-node-https" {
  description              = "Allow pods to communicate with the cluster API Server"
  from_port                = 443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.cluster.id
  source_security_group_id = aws_security_group.cluster-node.id
  to_port                  = 443
  type                     = "ingress"
}
